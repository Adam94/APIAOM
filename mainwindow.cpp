#include "vtkAutoInit.h"
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QString>
#include <vtkImageData.h>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent,
                       std::unique_ptr<IImageViewer> p_imageViewer,
                       vtkSmartPointer<vtkImageViewer2> p_proxy) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_imageViewer(std::move(p_imageViewer)),
    m_proxy(std::move(p_proxy)),
    m_isBrain3DMode(true)
{
    qDebug() << "Przed setupem UI";
    ui->setupUi(this);
    this->setWindowTitle(trUtf8("Medical Image Viewer"));

    m_renderWindow = ui->qvtkWidget->GetRenderWindow();
    m_imageViewer->setRenderWindow(m_renderWindow);
    m_imageViewer->changeOrientation(TPlane::BRAIN3D);

    ui->actionKosci->setToolTip("W danych CT pozwala na wyekstrahowanie części odpowiedzialnych za widok kości");
    ui->actionSkora->setToolTip("W danych CT pozwala na wyekstrahowanie części odpowiedzialnych za widok skóry");
    ui->sagittalSlider->setToolTip("Suwak kolejnych slajsów w płaszczyźnie strzałkowej");
    ui->coronalSlider->setToolTip("Suwak kolejnych slajsów w płaszczyźnie czołowej");
    ui->axialSlider->setToolTip("Suwak kolejnych slajsów w płaszczyźnie poprzecznej");
}

void MainWindow::on_actionWczytaj_Dicom_triggered()
{
    qDebug() << "on_actionWczytaj_Dicom_triggered";
    QString l_filename = QFileDialog::getOpenFileName(this, "File with medical image", "/home",
                                                      tr("Medical Image Files (*.mhd)"));
    if(l_filename.isEmpty())
    {
        return;
    }

    vtkSmartPointer<vtkMetaImageReader> l_reader =
        vtkSmartPointer<vtkMetaImageReader>::New();

    l_reader->SetFileName(l_filename.toStdString().c_str());
    l_reader->Update();
    m_imageViewer->setInputConnection(l_reader->GetOutputPort());
    m_proxy->SetInputConnection(l_reader->GetOutputPort());

    auto l_images = m_proxy->GetInput();
    int l_sliceDimension[3];
    l_images->GetDimensions(l_sliceDimension);

    ui->axialSlider->setMaximum(l_sliceDimension[2]);
    ui->coronalSlider->setMaximum(l_sliceDimension[1]);
    ui->sagittalSlider->setMaximum(l_sliceDimension[0]);

    m_imageViewer->render();
}

void MainWindow::on_actionWidok_pierwszy_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::AXIAL);
    ui->axialSlider->show();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->axialSlider->setToolTip("Suwak kolejnych slajsów w płaszczyźnie poprzecznej");
}

void MainWindow::on_actionWidok_drugi_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::CORONAL);
    ui->axialSlider->show();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->axialSlider->setToolTip("Suwak kolejnych slajsów w płaszczyźnie czołowej");
}

void MainWindow::on_actionWidok_trzeci_triggered()
{
    m_isBrain3DMode = false;
    m_imageViewer->changeOrientation(TPlane::SAGITAL);
    ui->axialSlider->show();
    ui->coronalSlider->hide();
    ui->sagittalSlider->hide();
    ui->axialSlider->setToolTip("Suwak kolejnych slajsów w płaszczyźnie strzałkowej");
}

void MainWindow::on_actionBrain3D_triggered()
{
    m_isBrain3DMode = true;
    m_imageViewer->changeOrientation(TPlane::BRAIN3D);
    ui->axialSlider->show();
    ui->coronalSlider->show();
    ui->sagittalSlider->show();
}

void MainWindow::on_axialSlider_valueChanged(int p_slice)
{
    if(m_isBrain3DMode == false)
    {
        m_imageViewer->setSlice(p_slice);
    }
    else
    {
        m_imageViewer->setSlice(p_slice, TPlane::AXIAL);
    }
    m_imageViewer->render();
}

void MainWindow::on_coronalSlider_valueChanged(int p_slice)
{
    if(m_isBrain3DMode == false)
    {
        m_imageViewer->setSlice(p_slice);
    }
    else
    {
        m_imageViewer->setSlice(p_slice, TPlane::CORONAL);
    }
    m_imageViewer->render();
}

void MainWindow::on_sagittalSlider_valueChanged(int p_slice)
{

    if(m_isBrain3DMode == false)
    {
        m_imageViewer->setSlice(p_slice);
    }
    else
    {
        m_imageViewer->setSlice(p_slice, TPlane::SAGITAL);
    }
    m_imageViewer->render();
}

void MainWindow::on_actionSkora_triggered()
{
    m_imageViewer->setMode(TMode::skin);
    m_imageViewer->render();
}

void MainWindow::on_actionKosci_triggered()
{
    m_imageViewer->setMode(TMode::bone);
    m_imageViewer->render();
}

void MainWindow::on_actionWidok_1_triggered()
{
    m_imageViewer->changeStateOfSliceExplorer(TPlane::AXIAL);
    m_imageViewer->render();
}

void MainWindow::on_actionWidok_2_triggered()
{
    m_imageViewer->changeStateOfSliceExplorer(TPlane::CORONAL);
    m_imageViewer->render();
}

void MainWindow::on_actionWidok_3_triggered()
{
    m_imageViewer->changeStateOfSliceExplorer(TPlane::SAGITAL);
    m_imageViewer->render();
}

void MainWindow::on_actionO_Qt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_actionO_Autorze_triggered()
{
    QMessageBox::about(this, "Kontakt do autora aplikacji", "Klimczak Adam - klimczak.a@o2.pl \nAlgorytmy przetwarzania i analizy obrazów medycznych 2017/2018");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionZamknij_triggered()
{
    qApp->exit();
}
