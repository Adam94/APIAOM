#ifndef IIMAGEVIEWER_H
#define IIMAGEVIEWER_H

#include <vtkRenderWindow.h>
#include <vtkAlgorithmOutput.h>

enum TPlane
{
    AXIAL,
    SAGITAL,
    CORONAL,
    BRAIN3D
};

enum class TMode
{
    bone,
    skin
};

struct WindowSize
{
    int height;
    int width;
};

class IImageViewer
{
public:
    ~IImageViewer() = default;

    virtual void setInputConnection(vtkAlgorithmOutput* p_images) = 0;
    virtual void setRenderWindow(vtkRenderWindow *p_renderWindow) = 0;
    virtual void setSlice(int p_slice) = 0;
    virtual void setSlice(int p_slice, TPlane p_mode) = 0;

    virtual void changeOrientation(TPlane p_plane) = 0;
    virtual void render() = 0;

    virtual void setMode(TMode) = 0;

    virtual int getSliceMax() = 0;
    virtual int getSliceMin() = 0;

    virtual void changeStateOfSliceExplorer(TPlane p_plane) = 0;
};

#endif // IIMAGEVIEWER_H
