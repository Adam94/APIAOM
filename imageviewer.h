#pragma once

#include "iimageViewer.h"

#include <vtkImageViewer2.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkMetaImageReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkOutlineFilter.h>
#include <vtkCamera.h>
#include <vtkProperty.h>
#include <vtkMarchingCubes.h>
#include <vtkSmartPointer.h>
#include <vtkStripper.h>
#include <vtkLookupTable.h>
#include <vtkImageMapToColors.h>
#include <vtkImageActor.h>
#include <vtkImageMapper3D.h>

#include <bitset>

namespace
{
    struct montageParameters
    {
        int firstSlideIndex;
        int noRows;
        int noCols;
        WindowSize windowSize;
    };
}

class imageViewer : public IImageViewer
{
public:
    imageViewer(vtkSmartPointer<vtkImageViewer2> p_viewer =
                    vtkSmartPointer<vtkImageViewer2>::New(),
                vtkSmartPointer<vtkRenderer> p_renderer =
                    vtkSmartPointer<vtkRenderer>::New(),
                vtkSmartPointer<vtkMarchingCubes> p_skinExtractor =
                    vtkSmartPointer<vtkMarchingCubes>::New(),
                vtkSmartPointer<vtkStripper> p_skinStripper =
                    vtkSmartPointer<vtkStripper>::New(),
                vtkSmartPointer<vtkPolyDataMapper> p_skinMapper =
                    vtkSmartPointer<vtkPolyDataMapper>::New(),
                vtkSmartPointer<vtkActor> p_skin =
                    vtkSmartPointer<vtkActor>::New(),
                vtkSmartPointer<vtkMarchingCubes> p_boneExtractor =
                    vtkSmartPointer<vtkMarchingCubes>::New(),
                vtkSmartPointer<vtkStripper> boneStripper =
                    vtkSmartPointer<vtkStripper>::New(),
                vtkSmartPointer<vtkPolyDataMapper> p_boneMapper =
                    vtkSmartPointer<vtkPolyDataMapper>::New(),
                vtkSmartPointer<vtkActor> p_bone =
                    vtkSmartPointer<vtkActor>::New(),
                vtkSmartPointer<vtkLookupTable> p_bwLut =
                    vtkSmartPointer<vtkLookupTable>::New(),
                vtkSmartPointer<vtkLookupTable> p_hueLut =
                    vtkSmartPointer<vtkLookupTable>::New(),
                vtkSmartPointer<vtkLookupTable> p_satLut =
                    vtkSmartPointer<vtkLookupTable>::New(),
                vtkSmartPointer<vtkImageMapToColors> p_sagittalColors =
                    vtkSmartPointer<vtkImageMapToColors>::New(),
                vtkSmartPointer<vtkImageMapToColors> p_axialColors =
                    vtkSmartPointer<vtkImageMapToColors>::New(),
                vtkSmartPointer<vtkImageMapToColors> p_coronalColors =
                    vtkSmartPointer<vtkImageMapToColors>::New(),
                vtkSmartPointer<vtkImageActor> p_sagittal =
                    vtkSmartPointer<vtkImageActor>::New(),
                vtkSmartPointer<vtkImageActor> p_axial =
                    vtkSmartPointer<vtkImageActor>::New(),
                vtkSmartPointer<vtkImageActor> p_coronal =
                    vtkSmartPointer<vtkImageActor>::New(),
                vtkSmartPointer<vtkOutlineFilter> p_outlineData =
                     vtkSmartPointer<vtkOutlineFilter>::New(),
                vtkSmartPointer<vtkPolyDataMapper> p_mapOutline =
                     vtkSmartPointer<vtkPolyDataMapper>::New(),
                vtkSmartPointer<vtkActor> p_outline =
                     vtkSmartPointer<vtkActor>::New(),
                vtkSmartPointer<vtkCamera> p_camera =
                    vtkSmartPointer<vtkCamera>::New());

    void setInputConnection(vtkAlgorithmOutput* p_images) override;
    void setRenderWindow(vtkRenderWindow *p_renderWindow) override;
    void setSlice(int p_slice) override;
    void setSlice(int p_slice, TPlane p_mode) override;

    void setMode(TMode) override;

    void changeOrientation(TPlane p_plane) override;
    void render() override;
    int getSliceMax() override;
    int getSliceMin() override;

    void changeStateOfSliceExplorer(TPlane p_plane) override;
private:
    void changeStateOfSliceExplorer(TPlane p_plane,
                                    vtkSmartPointer<vtkImageActor> p_actor);


    vtkRenderWindow* m_renderWindow;
    vtkSmartPointer<vtkImageViewer2> m_vtkViewer;
    vtkSmartPointer<vtkRenderer> m_renderer;

    vtkSmartPointer<vtkMarchingCubes> m_skinExtractor;
    vtkSmartPointer<vtkStripper> m_skinStripper;
    vtkSmartPointer<vtkPolyDataMapper> m_skinMapper;
    vtkSmartPointer<vtkActor> m_skin;

    vtkSmartPointer<vtkMarchingCubes> m_boneExtractor;
    vtkSmartPointer<vtkStripper> m_boneStripper;
    vtkSmartPointer<vtkPolyDataMapper> m_boneMapper;
    vtkSmartPointer<vtkActor> m_bone;

    vtkSmartPointer<vtkLookupTable> m_bwLut;
    vtkSmartPointer<vtkLookupTable> m_hueLut;
    vtkSmartPointer<vtkLookupTable> m_satLut;

    vtkSmartPointer<vtkImageMapToColors> m_sagittalColors;
    vtkSmartPointer<vtkImageMapToColors> m_axialColors;
    vtkSmartPointer<vtkImageMapToColors> m_coronalColors;

    vtkSmartPointer<vtkImageActor> m_sagittal;
    vtkSmartPointer<vtkImageActor> m_axial;
    vtkSmartPointer<vtkImageActor> m_coronal;

    vtkSmartPointer<vtkOutlineFilter> m_outlineData;
    vtkSmartPointer<vtkPolyDataMapper> m_mapOutline;
    vtkSmartPointer<vtkActor> m_outline;

    vtkSmartPointer<vtkCamera> m_camera;

    TPlane m_view;
    std::bitset<3> m_SliceExplorerStates;

};
