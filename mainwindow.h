#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "imageviewer.h"
#include <vtkSmartPointer.h>
#include <vtkImageViewer2.h>
#include <QMainWindow>
#include <QtDebug>
#include <QVTKWidget.h>
#include <QFileDialog>
#include <memory>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0,
               std::unique_ptr<IImageViewer> p_imageViewer =
                    std::make_unique<imageViewer>(),
               vtkSmartPointer<vtkImageViewer2> p_proxy =
                    vtkSmartPointer<vtkImageViewer2>::New());
    ~MainWindow();

private slots:
    void on_actionWidok_pierwszy_triggered();
    void on_actionWidok_drugi_triggered();
    void on_actionWidok_trzeci_triggered();
    void on_actionBrain3D_triggered();
    void on_actionZamknij_triggered();
    void on_actionWczytaj_Dicom_triggered();
    void on_axialSlider_valueChanged(int p_slice);
    void on_coronalSlider_valueChanged(int p_slice);
    void on_sagittalSlider_valueChanged(int p_slice);
    void on_actionSkora_triggered();
    void on_actionKosci_triggered();
    void on_actionWidok_1_triggered();
    void on_actionWidok_2_triggered();
    void on_actionWidok_3_triggered();
    void on_actionO_Qt_triggered();
    void on_actionO_Autorze_triggered();

private:
    Ui::MainWindow *ui;
    vtkRenderWindow* m_renderWindow;
    std::unique_ptr<IImageViewer> m_imageViewer;
    vtkSmartPointer<vtkImageViewer2> m_proxy;
    bool m_isBrain3DMode;
};

#endif // MAINWINDOW_H
