#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    #ifndef QT_DEBUG
        vtkObject::GlobalWarningDisplayOff();
    #endif

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
