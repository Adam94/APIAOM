#include "imageviewer.h"

#include <vtkImageData.h>
#include <QtDebug>
#include <vtkDataSetMapper.h>
#include <vtkActor2D.h>
#include <vtkImageMapper.h>
#include <vtkImageResize.h>
#include <vtkActor2DCollection.h>

imageViewer::imageViewer(vtkSmartPointer<vtkImageViewer2> p_viewer,
                         vtkSmartPointer<vtkRenderer> p_renderer,
                         vtkSmartPointer<vtkMarchingCubes> p_skinExtractor,
                         vtkSmartPointer<vtkStripper> p_skinStripper,
                         vtkSmartPointer<vtkPolyDataMapper> p_skinMapper,
                         vtkSmartPointer<vtkActor> p_skin,
                         vtkSmartPointer<vtkMarchingCubes> p_boneExtractor,
                         vtkSmartPointer<vtkStripper> p_boneStripper,
                         vtkSmartPointer<vtkPolyDataMapper> p_boneMapper,
                         vtkSmartPointer<vtkActor> p_bone,
                         vtkSmartPointer<vtkLookupTable> p_bwLut,
                         vtkSmartPointer<vtkLookupTable> p_hueLut,
                         vtkSmartPointer<vtkLookupTable> p_satLut,
                         vtkSmartPointer<vtkImageMapToColors> p_sagittalColors,
                         vtkSmartPointer<vtkImageMapToColors> p_axialColors,
                         vtkSmartPointer<vtkImageMapToColors> p_coronalColors,
                         vtkSmartPointer<vtkImageActor> p_sagittal,
                         vtkSmartPointer<vtkImageActor> p_axial,
                         vtkSmartPointer<vtkImageActor> p_coronal,
                         vtkSmartPointer<vtkOutlineFilter> p_outlineData,
                         vtkSmartPointer<vtkPolyDataMapper> p_mapOutline,
                         vtkSmartPointer<vtkActor> p_outline,
                         vtkSmartPointer<vtkCamera> p_camera) :
    m_vtkViewer{std::move(p_viewer)},
    m_renderer{std::move(p_renderer)},
    m_skinExtractor{std::move(p_skinExtractor)},
    m_skinStripper{std::move(p_skinStripper)},
    m_skinMapper{std::move(p_skinMapper)},
    m_skin{std::move(p_skin)},
    m_boneExtractor{std::move(p_boneExtractor)},
    m_boneStripper{std::move(p_boneStripper)},
    m_boneMapper{std::move(p_boneMapper)},
    m_bone{std::move(p_bone)},
    m_bwLut{std::move(p_bwLut)},
    m_hueLut{std::move(p_hueLut)},
    m_satLut{std::move(p_satLut)},
    m_sagittalColors{std::move(p_sagittalColors)},
    m_axialColors{std::move(p_axialColors)},
    m_coronalColors{std::move(p_coronalColors)},
    m_sagittal{std::move(p_sagittal)},
    m_axial{std::move(p_axial)},
    m_coronal{std::move(p_coronal)},
    m_outlineData{std::move(p_outlineData)},
    m_mapOutline{std::move(p_mapOutline)},
    m_outline{std::move(p_outline)},
    m_camera{std::move(p_camera)},
    m_view{TPlane::AXIAL},
    m_SliceExplorerStates{std::bitset<3>("111")}
{
    m_skinExtractor->SetValue(0, 500);
    m_skinExtractor->Update();

    m_skinStripper->SetInputConnection(m_skinExtractor->GetOutputPort());
    m_skinStripper->Update();

    m_skinMapper->SetInputConnection(m_skinStripper->GetOutputPort());
    m_skinMapper->ScalarVisibilityOff();

    m_skin->SetMapper(m_skinMapper);
    m_skin->GetProperty()->SetDiffuseColor(1, .49, .25);
    m_skin->GetProperty()->SetSpecular(.3);
    m_skin->GetProperty()->SetSpecularPower(20);

    m_boneExtractor->SetValue(0,1150);

    m_boneStripper->SetInputConnection(m_boneExtractor->GetOutputPort());

    m_boneMapper->SetInputConnection(m_boneStripper->GetOutputPort());
    m_boneMapper->ScalarVisibilityOff();

    m_bone->SetMapper(m_boneMapper);
    m_bone->GetProperty()->SetDiffuseColor(1, 1, .9412);

    m_sagittalColors->SetLookupTable(m_bwLut);
    m_sagittalColors->Update();

    m_sagittal->GetMapper()->SetInputConnection(m_sagittalColors->GetOutputPort());

    m_axialColors->SetLookupTable(m_hueLut);
    m_axialColors->Update();

    m_axial->GetMapper()->SetInputConnection(m_axialColors->GetOutputPort());

    m_coronalColors->SetLookupTable(m_satLut);
    m_coronalColors->Update();

    m_coronal->GetMapper()->SetInputConnection(m_coronalColors->GetOutputPort());

    m_outlineData->Update();

    m_mapOutline->SetInputConnection(m_outlineData->GetOutputPort());

    m_outline->SetMapper(m_mapOutline);
    m_outline->GetProperty()->SetColor(0,0,0);

    m_camera->SetViewUp (0, 0, -1);
    m_camera->SetPosition (0, -1, 0);
    m_camera->SetFocalPoint (0, 0, 0);
    m_camera->ComputeViewPlaneNormal();
    m_camera->Azimuth(30.0);
    m_camera->Elevation(30.0);

    m_renderer->SetBackground(0.1, 0.2, 0.3);
    m_renderer->AddActor(m_outline);
    m_renderer->AddActor(m_sagittal);
    m_renderer->AddActor(m_axial);
    m_renderer->AddActor(m_coronal);
    m_renderer->AddActor(m_skin);
    m_renderer->AddActor(m_bone);

    m_renderer->SetActiveCamera(m_camera);
    m_renderer->ResetCamera();

    m_bone->VisibilityOff();
    m_skin->GetProperty()->SetOpacity(0.5);
    m_bone->GetProperty()->SetOpacity(0.5);
    m_camera->Dolly(1.5);
}

void imageViewer::setRenderWindow(vtkRenderWindow* p_renderWindow)
{
    m_renderWindow = p_renderWindow;
    m_vtkViewer->SetRenderWindow(p_renderWindow);
    m_renderer->SetRenderWindow(m_renderWindow);
}

void imageViewer::setInputConnection(vtkAlgorithmOutput* p_images)
{
    m_skinExtractor->SetInputConnection(p_images);
    m_boneExtractor->SetInputConnection(p_images);
    m_outlineData->SetInputConnection(p_images);
    m_sagittalColors->SetInputConnection(p_images);
    m_axialColors->SetInputConnection(p_images);
    m_coronalColors->SetInputConnection(p_images);
    m_vtkViewer->SetInputConnection(p_images);

    auto l_image = m_vtkViewer->GetInput();
    double l_range[2];
    l_image->GetScalarRange(l_range);
    m_bwLut->SetTableRange (l_range[0], l_range[1]);
    m_bwLut->SetSaturationRange (0, 0);
    m_bwLut->SetHueRange (0, 0);
    m_bwLut->SetValueRange (0, 1);
    m_bwLut->Build();

    m_hueLut->SetTableRange (l_range[0], l_range[1]);
    m_hueLut->SetSaturationRange (0, 0);
    m_hueLut->SetHueRange (0, 0);
    m_hueLut->SetValueRange (0, 1);
    m_hueLut->Build();

    m_satLut->SetTableRange (l_range[0], l_range[1]);
    m_satLut->SetSaturationRange (0, 0);
    m_satLut->SetHueRange (0, 0);
    m_satLut->SetValueRange (0, 1);
    m_satLut->Build();

    int l_slicesRange[3];
    l_image->GetDimensions(l_slicesRange);
    m_sagittal->SetDisplayExtent(l_slicesRange[0]/2, l_slicesRange[0]/2,
                                 0,l_slicesRange[1],
                                 0,l_slicesRange[2]);

    m_coronal->SetDisplayExtent(0, l_slicesRange[0],
                                l_slicesRange[1]/2, l_slicesRange[1]/2,
                                0,l_slicesRange[2]);

    m_axial->SetDisplayExtent(0, l_slicesRange[0],
                              0, l_slicesRange[1],
                              l_slicesRange[2]/2, l_slicesRange[2]/2);
}

void imageViewer::changeOrientation(TPlane p_plane)
{
    m_renderWindow->RemoveRenderer(m_renderer);
    if(p_plane == TPlane::AXIAL)
    {
        m_vtkViewer->SetSliceOrientationToXY();
        m_view = TPlane::AXIAL;
    }
    if(p_plane == TPlane::CORONAL)
    {
        m_vtkViewer->SetSliceOrientationToXZ();
        m_view = TPlane::CORONAL;
    }
    if(p_plane == TPlane::SAGITAL)
    {
        m_vtkViewer->SetSliceOrientationToYZ();
        m_view = TPlane::SAGITAL;
    }
    if(p_plane == TPlane::BRAIN3D)
    {
        m_renderWindow->AddRenderer(m_renderer);
        m_view = TPlane::BRAIN3D;
        m_renderer->SetBackground(0.1, 0.2, 0.3);

        m_renderer->RemoveActor(m_skin);
        m_renderer->RemoveActor(m_bone);

        m_renderer->AddActor(m_axial);
        m_renderer->AddActor(m_coronal);
        m_renderer->AddActor(m_sagittal);
        m_renderer->AddActor(m_skin);
        m_renderer->AddActor(m_bone);

        m_SliceExplorerStates.set();
    }
}

void imageViewer::render()
{
    m_vtkViewer->Render();
}

void imageViewer::setSlice(int p_slice)
{
    m_vtkViewer->SetSlice(p_slice);
}

void imageViewer::setSlice(int p_slice, TPlane p_mode)
{
    auto l_image = m_vtkViewer->GetInput();
    int l_slicesRange[3];
    l_image->GetDimensions(l_slicesRange);

    if(p_mode == TPlane::SAGITAL)
    {
        m_sagittal->SetDisplayExtent(p_slice, p_slice,
                                     0,l_slicesRange[1],
                                     0,l_slicesRange[2]);
    }

    if(p_mode == TPlane::CORONAL)
    {
        m_coronal->SetDisplayExtent(0, l_slicesRange[0],
                                    p_slice, p_slice,
                                    0,l_slicesRange[2]);
    }

    if(p_mode == TPlane::AXIAL)
    {
        m_axial->SetDisplayExtent(0, l_slicesRange[0],
                                  0, l_slicesRange[1],
                                  p_slice, p_slice);
    }
}

void imageViewer::setMode(TMode p_mode)
{
    if(p_mode == TMode::bone)
    {
        m_bone->VisibilityOn();
        m_skin->VisibilityOff();
    }
    else
    {
        m_bone->VisibilityOff();
        m_skin->VisibilityOn();
    }
}

int imageViewer::getSliceMax()
{
    return m_vtkViewer->GetSliceMax();
}

int imageViewer::getSliceMin()
{
    return m_vtkViewer->GetSliceMin();
}

void imageViewer::changeStateOfSliceExplorer(TPlane p_plane,
                                             vtkSmartPointer<vtkImageActor> p_actor)
{
    if(m_SliceExplorerStates[p_plane])
    {
       m_renderer->RemoveActor(p_actor);
       m_SliceExplorerStates.reset(p_plane);
    }
    else
    {
        m_renderer->RemoveActor(m_skin);
        m_renderer->RemoveActor(m_bone);

        m_renderer->AddActor(p_actor);
        m_SliceExplorerStates.set(p_plane);
        m_renderer->AddActor(m_skin);
        m_renderer->AddActor(m_bone);
    }
}

void imageViewer::changeStateOfSliceExplorer(TPlane p_plane)
{
    if(p_plane == TPlane::AXIAL)
    {
        changeStateOfSliceExplorer(TPlane::AXIAL, m_axial);
        return;
    }
    if(p_plane == TPlane::SAGITAL)
    {
        changeStateOfSliceExplorer(TPlane::SAGITAL, m_sagittal);
        return;
    }
    if(p_plane == TPlane::CORONAL)
    {
        changeStateOfSliceExplorer(TPlane::CORONAL, m_coronal);
        return;
    }
}
